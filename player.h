#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

private: 
	Side * mySide; 
	Side * otherSide; 
	static const int weights[8][8];

public:
	Board * board;
    Player(Side side);
    ~Player();
    int getScore(Side side, Board board1); 
	int getMinimaxScore(Side side, Board board1);
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
