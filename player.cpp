#include <stdio.h>
#include "player.h"
 

const int Player::weights[8][8] =  {{5, -4, 2, 2, 2, 2, -4, 5}, {-4, -5, -1, -1, -1, -1, -5, -4},{2, -1, 1, 0, 0, 1, -1, 2}, {2, -1, 0, 1, 1, 0, -1, 2}, {2, -1, 0, 1, 1, 0, -1, 2}, {2, -1, 1, 0, 0, 1, -1, 2}, {-4, -5, -1, -1, -1, -1, -5, -4},{5,-4, 2, 2, 2, 2, -4, 5}};
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	//Create an approprate board
	board = new Board(); 

	// Initialize the side 
	mySide = new Side();
	*mySide = side;

	otherSide = new Side();
	if (side == BLACK) {
		*otherSide = WHITE; 
	} 
	else {
		*otherSide = BLACK;
	} 

}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/* Gets the score of the board using a heuristic function 
 * given the side, the state of the board, and the move to 
 * perform 
 */ 
int Player::getScore(Side side, Board board1){ 
	
	int score = 0;
		
	for (int i = 0; i < 8; i ++) {
		for (int j = 0; j < 8; j ++) {
			// If a spot is occuppied by our side 
			if (board1.get(side, i, j)) { 
				// Get the appropriate weight 
				score += weights[i][j];
			} 
		} 
	} 
	return score;

}	

int Player::getMinimaxScore(Side side, Board board1) {
	int score = 0;	
	if (side == WHITE) {
		score = board1.countWhite() - board1.countBlack(); 
	} 
	else {
		score = board1.countBlack() - board1.countWhite();
	} 
	 
	return score; 
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	
	Move * nextMove;
	Move * secondMove;
	
	int bestScore = -100000;
	int minSecondBranch; 

	Move * bestMove = NULL; 
	Board * bc; 
	Board * bc2;
	int score; 	

	// If your opponents move is null this means the opponent did 
	// not do anything and so we must simply choose a move. Otherwise 
	// we must update the board.	
	if (opponentsMove != NULL) {
		board->doMove(opponentsMove, *otherSide); 
	}
	// We must now consider our own moves. Let us go through every 
	// possible move in the board and check whether it is valid. 
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
		    nextMove = new Move(i, j);
			// If we have a valid move for our side we can consider this 
		    if (board->checkMove(nextMove, *mySide)){
				// Let us perform the move on a copy of the board 
				bc = board->copy(); 
				bc->doMove(nextMove, *mySide);
				minSecondBranch = 100000; 
				// We must now consider every subsequent move
				for (int m = 0; m < 8; m ++) {
					for (int n = 0; n < 8; n ++) {
						secondMove = new Move(m, n); 
						// If the opponent is allowed to make the 
						// second move we must make a board copy
						if (bc->checkMove(secondMove, *otherSide)) {
							bc2 = bc->copy();
							bc2->doMove(secondMove, *otherSide); 
							// Now let us get our score 
							if (testingMinimax == true) {
								score = getMinimaxScore(*mySide, *bc2);
							} 
							else {
								score = getScore(*mySide, *bc2); 
							}  
							// If the score is the least for this 
							// branch we must note this.
							if (score < minSecondBranch) {
								minSecondBranch = score; 
							} 
						} 
					} 
				}
				//minSecondBranch now contains the smallest score 
				//we can get in this branch. We want to maximize 
				//this value for all the moves. 
				if (minSecondBranch > bestScore) {
					bestScore = minSecondBranch; 
					bestMove = nextMove;
				} 
			} 
		}
	}
	
	board->doMove(bestMove, *mySide);
	return bestMove; 		
	
}
