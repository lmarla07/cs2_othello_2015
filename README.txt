In this readme I am to describe how I made improvements to win the
tournament. 

I attempted to implement the recursive solution to the minimax but this
solution did not seem to work (I kept getting segfaults for some reason). My 
heuristic consistently wins against constanttimeplayer and ties with 
betterplayer. Any improvements I made to my heuristic (changing the weighting
to increase the values at the corners dramatically) did not change the results
dramatically against any of the players from last week, and made 
the results with BetterPlayer worse this week. 

My strategy should work relatively well because the heuristic I have 
constructed seems very good, or at least good enough to tie a three depth 
minimax tree. 

